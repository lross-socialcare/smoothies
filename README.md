
### What is this repository for? ###

To demonstrate a proposed back-end service project structure & interactions among such services.

### How do I get set up? ###

Run Dockerfiles in three repos:

* smoothies
* do-you-like-apples
* hey-mister-tally-man
