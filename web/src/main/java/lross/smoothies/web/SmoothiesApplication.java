package lross.smoothies.web;

import lross.smoothies.service.SmoothieServiceSpringConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({SmoothieServiceSpringConfiguration.class})
public class SmoothiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmoothiesApplication.class, args);
    }
}
