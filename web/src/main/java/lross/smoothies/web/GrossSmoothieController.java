package lross.smoothies.web;

import lombok.RequiredArgsConstructor;
import lross.smoothies.domain.GrossSmoothie;
import lross.smoothies.service.GrossSmoothieService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GrossSmoothieController {

    private final GrossSmoothieService grossSmoothieService;

    @RequestMapping("/")
    public GrossSmoothie getGrossSmoothie() {

        return grossSmoothieService.blend();
    }
}
