package lross.smoothies.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lross.doyoulikeapples.api.AppleApi;
import lross.doyoulikeapples.clientokhttp.AppleClient;
import lross.doyoulikeapples.domain.Apple;
import lross.heymistertallyman.api.BananaApi;
import lross.heymistertallyman.clientokhttp.BananaClient;
import lross.heymistertallyman.domain.Banana;
import lross.smoothies.domain.GrossSmoothie;
import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;

@Component
public class GrossSmoothieService {

    private final AppleApi appleApi;
    private final BananaApi bananaApi;

    GrossSmoothieService(
            OkHttpClient okHttpClient,
            ObjectMapper objectMapper) {

        this.appleApi = new AppleClient(okHttpClient, objectMapper, "apples", 8080);
        this.bananaApi = new BananaClient(okHttpClient, objectMapper, "bananas", 8080);
    }

    public GrossSmoothie blend() {

        Apple apple = appleApi.getApplesByVariety("Braeburn").stream().findAny()
                .orElseThrow(IllegalStateException::new);
        Banana banana = bananaApi.getRipeBananas().stream().findAny()
                .orElseThrow(IllegalStateException::new);

        return new GrossSmoothie(apple, banana);
    }
}
