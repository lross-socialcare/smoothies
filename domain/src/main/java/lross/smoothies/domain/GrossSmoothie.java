package lross.smoothies.domain;

import lombok.Data;
import lross.doyoulikeapples.domain.Apple;
import lross.heymistertallyman.domain.Banana;

@Data
public class GrossSmoothie {

    private final Apple apple;
    private final Banana banana;
}
